import { ActivatedRoute, convertToParamMap, Data, Params } from '@angular/router';

import { ClassTest } from '../class.test';
import { MockActivatedRoute } from './mock-activated-route.class';

const params: Params = { param: 'param' };
const data: Data = { data: 'data' };

describe('MockActivatedRoute', () => {
  let test: ClassTest<MockActivatedRoute>;

  beforeEach(() => {
    test = new ClassTest(MockActivatedRoute);
  });

  it('extends ActivatedRoute', () => {
    test.inheritsFrom(ActivatedRoute);
  });

  // #region .setParams(params)

  it('.setParams(params) set .params', () => {
    test.obj.setParams(params);
    test.hasObservableValue({ key: 'params', value: params });
  });

  it('.setParams(params) set .paramMap', () => {
    test.obj.setParams(params);
    test.hasObservableValue({ key: 'paramMap', value: convertToParamMap(params) });
  });

  it('.setParams(params) set .snapshot.params', () => {
    test.obj.setParams(params);
    test.hasValue({ key: 'params', value: params }, test.obj.snapshot);
  });

  it('.setParams(params) set .snapshot.paramMap', () => {
    test.obj.setParams(params);
    test.hasValue({ key: 'paramMap', value: convertToParamMap(params) }, test.obj.snapshot);
  });

  // #endregion

  // #region .setQueryParams(params)

  it('.setQueryParams(params) set .queryParams', () => {
    test.obj.setQueryParams(params);
    test.hasObservableValue({ key: 'queryParams', value: params });
  });

  it('.setQueryParams(params) set .queryParamMap', () => {
    test.obj.setQueryParams(params);
    test.hasObservableValue({ key: 'queryParamMap', value: convertToParamMap(params) });
  });

  it('.setQueryParams(params) set .snapshot.queryParams', () => {
    test.obj.setQueryParams(params);
    test.hasValue({ key: 'queryParams', value: params }, test.obj.snapshot);
  });

  it('.setQueryParams(params) set .snapshot.queryParamMap', () => {
    test.obj.setQueryParams(params);
    test.hasValue({ key: 'queryParamMap', value: convertToParamMap(params) }, test.obj.snapshot);
  });

  // #endregion

  // #region .setData(data)

  it('.setData(data) set .data', () => {
    test.obj.setData(data);
    test.hasObservableValue({ key: 'data', value: data });
  });

  it('.setData(data) set .snapshot.data', () => {
    test.obj.setData(data);
    test.hasValue({ key: 'data', value: data }, test.obj.snapshot);
  });

  // #endregion
});
