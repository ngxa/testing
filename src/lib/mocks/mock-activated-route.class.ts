import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  convertToParamMap,
  Data,
  ParamMap,
  Params
} from '@angular/router';
import { Observable, of as just } from 'rxjs';

/**
 * Contains the mocked information about a route associated with a component loaded in an outlet.
 */
export class MockActivatedRoute extends ActivatedRoute {
  private _params: Params = {};
  private _queryParams: Params = {};
  private _data: Data = {};

  /**
   * Contains the mocked information about a route associated with a component loaded in an outlet.
   */
  constructor() {
    super();
    this.setQueryParams({});
    this.setParams({});
    this.setData({});
  }

  /**
   * The current snapshot of this route.
   */
  get snapshot(): ActivatedRouteSnapshot {
    return {
      ...new ActivatedRouteSnapshot(),
      params: this._params,
      queryParams: this._queryParams,
      data: this._data,
      paramMap: this._paramMap,
      queryParamMap: this._queryParamMap
    };
  }

  /**
   * The matrix parameters scoped to this route.
   */
  get paramMap(): Observable<ParamMap> {
    return just(this._paramMap);
  }

  private get _paramMap(): ParamMap {
    return convertToParamMap(this._params);
  }

  /**
   * The query parameters shared by all the routes.
   */
  get queryParamMap(): Observable<ParamMap> {
    return just(this._queryParamMap);
  }

  private get _queryParamMap(): ParamMap {
    return convertToParamMap(this._queryParams);
  }

  /**
   * Set the matrix parameters scoped to this route.
   * @param params The params to set.
   */
  setParams(params: Params): void {
    this._params = params;
    this.params = just(params);
  }

  /**
   * Set the query parameters shared by all the routes.
   * @param params The params to set.
   */
  setQueryParams(params: Params): void {
    this._queryParams = params;
    this.queryParams = just(params);
  }

  /**
   * Set the static and resolved data of this route.
   * @param data The data to set.
   */
  setData(data: Data): void {
    this._data = data;
    this.data = just(data);
  }
}
