import { Type } from '@angular/core';

import { ClassTestAbstract } from './abstracts/class-test.abstract';

/**
 * Class helper to test basic classes.
 */
export class ClassTest<T extends object> extends ClassTestAbstract<T> {
  /**
   * Creates a class helper to test basic classes
   * @param clazz The class type to test.
   * @param [args] Args to build the class.
   */
  constructor(clazz: Type<T>, ...args: any[]) {
    super();
    this.obj = new clazz(...args);
  }
}
