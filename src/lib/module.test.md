# How to test a module

Given an Angular module.

```typescript
@NgModule(
  imports: [CommonModule, HttpClientModule, FeatureModule],
  declarations: [SomeComponent, SomeDirective, SomePipe],
  providers: [SomeService]
)
class TestModule {}
```

First you must define the `ModuleTest` object. In this case, we **must** delare a `TestBedConfig` object and pass it as param to the ModuleTest.

The TestBedConfig object needs all the imports, declarations or providers that the module uses. Otherwise the test will fire an error. `CommonModule` is always imported, so if it's the only dependency of the module the config can be omitted.

```typescript
describe('TestModule', () => {
  let test: ModuleTest<TestModule>;
  const config: TestBedConfig = new TestBedConfig();

  config.imports.push(HttpClientModule, FeatureModule);
  config.declarations.push(SomeComponent, SomeDirective, SomePipe);
  config.providers.push(SomeService);

  beforeEach(() => {
    test = new ModuleTest(TestModule, config);
  });

  ...
});
```

Then we can use all [common class helpers](https://ngxa.gitlab.io/testing/classes/ClassTest.html), as well all [specific Angular helpers](https://ngxa.gitlab.io/testing/classes/AngularTestAbstract.html). The module object is under `test.obj`.
