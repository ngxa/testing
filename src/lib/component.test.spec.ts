import { Component, DebugElement, Directive, Input } from '@angular/core';
import { ComponentFixture } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { TestBedConfig } from '@ngxa/utils';

import { AngularTestAbstract } from './abstracts/angular-test.abstract';
import { ClassTestAbstract } from './abstracts/class-test.abstract';
import { ComponentTest } from './component.test';
import { MockActivatedRoute } from './mocks/mock-activated-route.class';

const numP: number = 4;
const numChildren: number = 4;

@Directive({ selector: '[ngxaDirective]' })
class TestDirective {
  @Input()
  ngxaDirective!: any;
}

@Directive({ selector: '[ngxaDirective2]' })
class TestDirective2 {
  @Input()
  ngxaDirective2!: any;
}

@Component({
  selector: 'ngxa-test',
  template: `
    <h1 id="title">Title</h1>
    <div id="id1">
      <p [ngxaDirective]="classTest" class="c1 c3">Paragraph 1</p>
      <p class="c2" accesskey="h">Paragraph 2</p>
      <button class="c1 c2 c3" id="idClick" (click)="click($event)">Button 1</button>
      <p id="idMouseOver" (mouseover)="over($event)">Paragraph 3</p>
    </div>
    <div id="id2">
      <p ngxaDirective="directive"></p>
      <div id="id2-subId1">Divsub 1</div>
      <div id="id2-subId2">Divsub 2</div>
      <input type="text" name="username" />
    </div>
    <form>
      <a href="/" class="link link2" id="link">link</a>
      <input id="checkbox" type="checkbox" checked /> checkbox
    </form>
  `
})
class TestComponent {
  @Input() test?: string;

  ngOnInit(): void {}

  public click(event: Event): Event {
    return event;
  }

  public over(event: Event): Event {
    return event;
  }
}

@Component({
  selector: 'ngxa-other-test',
  template: ``
})
class OtherTestComponent {
  public click(event: Event): Event {
    return event;
  }

  public over(event: Event): Event {
    return event;
  }
}

describe('ComponentTest', () => {
  describe('without config', () => {
    let test: ComponentTest<OtherTestComponent>;

    beforeEach(() => {
      test = new ComponentTest(OtherTestComponent);
    });

    it('inherits from AngularTestAbstract', () => {
      test.inheritsFrom(AngularTestAbstract, test);
    });

    it('inherits from ClassTestAbstract', () => {
      test.inheritsFrom(ClassTestAbstract, test);
    });

    it('.testBed have provided ActivatedRoute', () => {
      test.isProvided(ActivatedRoute);
    });

    it('.fixture is setted with the ComponentFixture', () => {
      expect(test.fixture).toBeDefined();
      expect(test.fixture instanceof ComponentFixture).toBeTruthy();
      expect(test.fixture.componentInstance).toEqual(new OtherTestComponent());
    });

    it('.obj is setted with the component', () => {
      expect(test.obj).toEqual(new OtherTestComponent());
    });

    it('.activatedRoute is setted with the ActivatedRoute service', () => {
      expect(test.activatedRoute).toBeDefined();
      expect(test.activatedRoute instanceof ActivatedRoute).toBeTruthy();
      expect(test.activatedRoute instanceof MockActivatedRoute).toBeTruthy();
    });

    it('.activatedRoute is mocked with MockActivatedRoute', () => {
      expect(test.activatedRoute instanceof MockActivatedRoute).toBeTruthy();
    });
  });

  describe('with config', () => {
    let test: ComponentTest<TestComponent>;
    const config: TestBedConfig = new TestBedConfig();

    config.declarations.push(TestDirective, TestDirective2);

    beforeEach(() => {
      test = new ComponentTest(TestComponent, config);
    });

    it('inherits from AngularTestAbstract', () => {
      test.inheritsFrom(AngularTestAbstract, test);
    });

    it('inherits from ClassTestAbstract', () => {
      test.inheritsFrom(ClassTestAbstract, test);
    });

    it('.testBed have provided ActivatedRoute', () => {
      test.isProvided(ActivatedRoute);
    });

    it('.fixture is setted with the ComponentFixture', () => {
      expect(test.fixture).toBeDefined();
      expect(test.fixture instanceof ComponentFixture).toBeTruthy();
      expect(test.fixture.componentInstance).toEqual(new TestComponent());
    });

    it('.obj is setted with the component', () => {
      expect(test.obj).toEqual(new TestComponent());
    });

    it('.activatedRoute is setted with the ActivatedRoute service', () => {
      expect(test.activatedRoute).toBeDefined();
      expect(test.activatedRoute instanceof ActivatedRoute).toBeTruthy();
      expect(test.activatedRoute instanceof MockActivatedRoute).toBeTruthy();
    });

    it('.activatedRoute is mocked with MockActivatedRoute', () => {
      expect(test.activatedRoute instanceof MockActivatedRoute).toBeTruthy();
    });

    it('setInput({}) set the inputs and fires onInit', () => {
      expect(test.obj.test).toBeUndefined();
      test.setInput({ test: 'test' });
      test.hasValue({ key: 'test', value: 'test' });
    });

    // #region .hasElement(call, element?)

    it('.hasElement({selector}) test element exists by selector', () => {
      test.hasElement({ selector: 'h1' });
    });

    it('.hasElement({directive}) test element exists by directive', () => {
      test.hasElement({ directive: TestDirective });
    });

    it('.hasElement({selector}) returns array of DebugElement by selector', () => {
      const elements: DebugElement[] = test.hasElement({ selector: 'p' });

      expect(elements instanceof Array).toBeTruthy('Should returns an Array');
      expect(elements.length).toBe(numP);
      expect(elements[0] instanceof DebugElement).toBeTruthy(
        "Should returns a DebugElement's Array"
      );
    });

    it('.hasElement({directive}) returns array of DebugElement by directive', () => {
      const elements: DebugElement[] = test.hasElement({ directive: TestDirective });

      expect(elements instanceof Array).toBeTruthy('Should returns an Array');
      expect(elements.length).toBe(2);
      expect(elements[0] instanceof DebugElement).toBeTruthy(
        "Should returns a DebugElement's Array"
      );
    });

    // #endregion

    // #region .hasNotElement(call, element?)

    it('.hasNotElement({selector}) test element do not exists by selector', () => {
      test.hasNotElement({ selector: '.not-a-selector' });
    });

    it('.hasNotElement({directive}) test element do not exists by directive', () => {
      test.hasNotElement({ directive: TestDirective2 });
    });

    // #endregion

    // #region .hasNthElement(call, element?)

    it('.hasNthElement({selector}) test 1st element exists', () => {
      test.hasNthElement({ selector: 'p' });
    });

    it('.hasNthElement({selector, nth}) test Nth element exists', () => {
      test.hasNthElement({ selector: 'p', nth: numP });
    });

    it('.hasNthElement({selector, nth}) returns DebugElement', () => {
      expect(test.hasNthElement({ selector: 'p', nth: numP }) instanceof DebugElement).toBeTruthy();
    });

    // #endregion

    // #region .hasNotNthElement(call, element?)

    it('.hasNotNthElement({selector, nth}) test Nth element do not exists', () => {
      test.hasNotNthElement({ selector: 'p', nth: numP + 1 });
    });

    // #endregion

    // #region .hasElementValue(call, element?)

    it('.hasElementValue({selector, nth, value}) test element match value', () => {
      test.hasElementValue({ selector: 'p', nth: 2, value: 'Paragraph 2' });
    });

    it('.hasElementValue({selector, nth, value}) returns the value', () => {
      expect(test.hasElementValue({ selector: 'p', nth: 2, value: 'P' })).toEqual('Paragraph 2');
    });

    // #endregion

    // #region .hasNotElementValue(call, element?)

    it('.hasNotElementValue({selector, nth, value}) test element do not match value', () => {
      test.hasNotElementValue({ selector: 'p', nth: 2, value: 'Paragraph 1' });
    });

    // #endregion

    // #region .hasElementChildren(call, element?)

    it('.hasElementChildren({selector}) test element have children', () => {
      test.hasElementChildren({ selector: 'div' });
    });

    it('.hasElementChildren({selector}) returns array of children DebugElement', () => {
      const elements: DebugElement[] = test.hasElementChildren({ selector: 'div' });

      expect(elements instanceof Array).toBeTruthy('Should returns an Array');
      expect(elements.length).toBe(numChildren);
      expect(elements[0] instanceof DebugElement).toBeTruthy(
        "Should returns a DebugElement's Array"
      );
    });

    // #endregion

    // #region .hasNotElementChildren(call, element?)

    it('.hasNotElementChildren({selector}) test element do not have children', () => {
      test.hasNotElementChildren({ selector: 'h1' });
    });

    // #endregion

    // #region .hasElementAttribute(call, element?)

    it('.hasElementAttribute({selector}) test element have attribute', () => {
      test.hasElementAttribute({ selector: 'h1', attribute: 'id' });
    });

    it('.hasElementAttribute({selector}) returns attribute value', () => {
      expect(test.hasElementAttribute({ selector: 'h1', attribute: 'id' })).toEqual('title');
    });

    // #endregion

    // #region .hasElementAttribute(call, element?)

    it('.hasNotElementAttribute({selector, attribute}) test element do not have attribute', () => {
      test.hasNotElementAttribute({ selector: 'h1', attribute: 'class' });
    });

    // #endregion

    // #region .hasElementClass(call, element?)

    it('.hasElementClass({selector, cssClass}) test element have a class', () => {
      test.hasElementClass({ selector: 'button', cssClass: 'c1' });
    });

    // #endregion

    // #region .hasNotElementClass(call, element?)

    it('.hasNotElementClass({selector, cssClass}) test element do not have a class', () => {
      test.hasNotElementClass({ selector: 'button', cssClass: 'c5' });
    });

    // #endregion

    // #region .eventCall(call, obj?)

    it('.eventCall({selector, eventName, key}) test element call method on click', () => {
      test.eventCall({ selector: '#idClick', eventName: 'click', key: 'click' });
    });

    it('.eventCall({selector, eventName, key, eventObj}) test element call method on click with event object', () => {
      test.eventCall({
        selector: '#idClick',
        eventName: 'click',
        key: 'click',
        eventObj: new Event('a')
      });
    });

    // #endregion

    // #region .eventNotCall(call, obj?)

    it('.eventNotCall({selector, event, key}) test element do not call method on click', () => {
      test.eventNotCall({ selector: '#idClick', eventName: 'click', key: 'over' });
    });

    // #endregion
  });
});
