# How to test a class

Given a basic class:

```typescript
class BasicClass {
  public propNumber?: number;
  public propString?: string;
  public propObject?: new OtherClass();

  public constructor(propNumber?: number, propString?: string) {
    this.propNumber = propNumber;
    this.propString = propString;
  }
}
```

First you must define the `ClassTest` object, passing the necessary parameters to instantiate it.

If you need to test different instances of objects, you must create the `test` object in each test.

```typescript
import { ClassTest } from '@ngxa/testing';
...

describe('BasicClass', () => {
  let test: ClassTest<BasicClass>;

  it('.obj is setted without args', () => {
    test = new ClassTest(BasicClass);
  });

  it('.obj is setted with args', () => {
    test = new ClassTest(BasicClass, 1, 'a');
  });

  ...
}
```

If all tests will use the same `test` object, an instance must be created in the `beforeEach` stage.

```typescript
import { ClassTest } from '@ngxa/testing';
...

describe('BasicClass', () => {
  let test: ClassTest<BasicClass>;

  beforeEach(() => {
    test = new ClassTest(BasicClass, 1, 'a');
  });

  ...
}
```

Once the `test` object is instantiated you can use all the related [helpers to test basic class](https://ngxa.gitlab.io/testing/classes/ClassTestAbstract.html) or create basic `expect`.

The object to be tested will always be a property in `test`, like `test.obj`. This object will always be the default target for helpers. If you want to use them to test internal objects, you must specify it.

```typescript
describe('BasicClass', () => {
  let test: ClassTest<BasicClass>;

  beforeEach(() => {
    test = new ClassTest(BasicClass, 1, 'a');
  });

  it('example using the obj with basic expects', () => {
    expect(test.obj.propNumber).toEqual(1);
  });

  it('example using test helpers', () => {
    test.isMember('propNumber');
  });

  it('example using test helpers with an inner object', () => {
    test.isMember('propNumber', this.obj.propObject);
  });

  ...
}
```
