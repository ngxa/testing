# How to test a component

First you must define the `ComponentTest` object. If the component have dependencies, we must create the `TestBedConfig` object and pass it as param to the ComponentTest. Otherwise, it can be omitted. It also provides a `MockActivatedRoute` that let you mock the `ActivatedRoute` service.

```typescript
describe('TestComponent', () => {
  let test: ComponentTest<TestComponent>;

  beforeEach(() => {
    test = new ComponentTest(TestComponent);
  });
});
```

Then we can use all [common class helpers](https://ngxa.gitlab.io/testing/classes/ClassTest.html), as well all [specific Angular helpers](https://ngxa.gitlab.io/testing/classes/AngularTestAbstract.html). The service object is under `test.obj`. In addition, we can use the next helpers.

## Template helpers

The following helpers let you assert that an HTML element exist. All helpers under this category have an optional `element` param, that accept a `DebugElement`, that defaults to the base component template root. You can filter any you want and use them as root using this param.

The Nth version have an optional `nth` parameter, that defaults to 1.

```ts
test.hasElement({ selector: 'p' }); // returns DebugElement[]
test.hasElement({ directive: TestDirective }); // returns DebugElement[]
test.hasNthElement({ selector: 'p', nth: 1 }); // returns DebugElement
test.hasNthElement({ directive: TestDirective, nth: 1 }); // returns DebugElement
```

```ts
const myDiv: DebugElement = test.hasNthElement({ selector: '#my-div' });

test.hasElement({ selector: 'p' }, myDiv);
// returns all p DebugElement[] under #my-div
```

These options uses `hasNthElement` to select the element to test. So you can use `selector` or `directive` to select the required, as well the `nth` param.

```ts
test.hasElementValue({ selector: 'p', value: 'a' });
test.hasElementChildren({ selector: 'div' });
test.hasElementAttribute({ selector: 'p', attribute: 'id' });
test.hasElementClass({ selector: 'p', cssClass: 'c1' });
```

## Events helpers

This helper let you check that an HTML event fires a class method. The optional `eventObj` param is the event value fired.

```ts
test.eventCall({ selector: '#idClick', eventName: 'click', key: 'click' });
test.eventCall({
  selector: '#idClick',
  eventName: 'click',
  key: 'click',
  eventObj: new Event('a')
});
```
