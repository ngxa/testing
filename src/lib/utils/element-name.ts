import { isNullOrUndefined } from 'util';

/**
 * Get the debug element name.
 * @param name The debug element name.
 * @returns The debug element name.
 */
export function elementName(name: string | null | undefined): string {
  return !isNullOrUndefined(name) && name !== '' ? name : 'component template';
}
