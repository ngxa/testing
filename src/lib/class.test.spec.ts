import { ClassTest } from './class.test';

class BasicClass {
  public propNumber: number | undefined;
  public propString: string | undefined;

  public constructor(propNumber?: number, propString?: string) {
    this.propNumber = propNumber;
    this.propString = propString;
  }
}

describe('ClassTest', () => {
  let test: ClassTest<BasicClass>;

  it('.obj is setted without args', () => {
    test = new ClassTest(BasicClass);
    expect(test.obj).toEqual(new BasicClass());
    expect(test.obj).not.toEqual(new BasicClass(1));
  });

  it('.obj is setted with one arg', () => {
    test = new ClassTest(BasicClass, 1);
    expect(test.obj).toEqual(new BasicClass(1));
    expect(test.obj).not.toEqual(new BasicClass());
  });

  it('.obj is setted with multiple args', () => {
    test = new ClassTest(BasicClass, 1, 'a');
    expect(test.obj).toEqual(new BasicClass(1, 'a'));
    expect(test.obj).not.toEqual(new BasicClass());
  });
});
