# Basic class helpers

This helpers can be used to test any type of basic typescript class and its members.

A Test Class Helper is needed to use them.

```ts
// Test Class Helper example
const test: ClassTest<AClass> = new ClassTest(AClass);
```

Once defined, they can be used as methods that will activate the `expect` functions to assert the described behavior. This function usually has a negative version that says otherwise. In this guide negative versions will be omitted, but you should remember they exists.

```ts
test.isMember('prop'); // positive
test.isNotMember('noProp'); // negative
```

They also use a last `obj` parameter to specify the object to be tested, which by default is the one that was tested. Sometimes we need to prove a member of the class built or injected. In this case, this last parameter will be the injected or inner class.

```ts
test.isMember('prop'); // the tested object
test.isMember('parentProp', ParentClass); // the subobject
```

It is also necessary to bear in mind that all methods verify the object and all the methods and properties in the hierarchy, so that the properties of a class of grandparents, for example, will be seen as their own.

```ts
// AClass extends GransParentClass
test.isMember('grandParentProp');
```

## Class helpers

This helpers check that the class itself has a characteristic.

```ts
test.isInstantiable();
test.inheritsFrom(ParentClassName);
```

## Members identity helpers

Identity helpers allow you to test and identify class members through object-oriented nomenclature. In ECMAScript the nomenclature is different, therefore, after these lines, it has a basic table with results to avoid confusion.

In addition, due to the limitations of the ECMAScript nature of the tested code, all undefined properties are taken as not existing, so they are not defined as members of the class.

```ts
class TestClass {
  public und: number;
  public puP: number = 0;
  protected prP: number = 0;
  private _pP: number = 0;
  public get pvP(): number {
    return this._pP;
  }
  public set pvP(value: number) {
    this._pP = value;
  }
  public get gtP(): number {
    return this._pP;
  }
  public set stP(value: number) {
    this._pP = value;
  }
  public puM(): number {
    return 0;
  }
  protected prM(): number {
    return 0;
  }
  private _pM(): number {
    return 0;
  }
  public fnM: function = (): number => 0;
}
```

|                | und | puP | prP | \_pP | pvP | gtP | stP | puM | prM | \_pM | fnM |
| -------------- | --- | --- | --- | ---- | --- | --- | --- | --- | --- | ---- | --- |
| isMember       | N   | Y   | Y   | Y    | Y   | Y   | Y   | Y   | Y   | Y    | Y   |
| isProperty     | N   | Y   | Y   | Y    | Y   | Y   | Y   | N   | N   | N    | N   |
| isGetterSetter | N   | N   | N   | N    | Y   | Y   | Y   | N   | N   | N    | N   |
| isGettable     | N   | Y   | Y   | Y    | Y   | Y   | N   | N   | N   | N    | N   |
| isGetter       | N   | N   | N   | N    | Y   | Y   | N   | N   | N   | N    | N   |
| isSettable     | N   | Y   | Y   | Y    | Y   | N   | Y   | N   | N   | N    | N   |
| isSetter       | N   | N   | N   | N    | Y   | N   | Y   | N   | N   | N    | N   |
| isMethod       | N   | N   | N   | N    | N   | N   | N   | Y   | Y   | Y    | Y   |

## Properties values

This helpers let you test the properties values and its type or instance. This ones also check that the key is a gettable property.

```ts
test.hasValue({ key: 'prop', value: 'a' });
test.hasValueType({ key: 'prop', typeOf: 'number' });
test.hasValueInstance({ key: 'prop', instanceOf: AClass });
```

You can also verify the value issued by an `Observable` without creating a new one, simply by specifying the value issued, as well as its type or type of instance. Observable errors are also allowed to be tested.

```ts
test.hasObservableValue({ key: 'prop', value: 'a' });
test.hasObservableValueType({ key: 'prop', typeOf: 'number' });
test.hasObservableValueInstance({ key: 'prop', instanceOf: AClass });
test.hasObservableError({ key: 'prop', error: 'a' });
test.hasObservableErrorType({ key: 'prop', typeOf: 'number' });
test.hasObservableErrorInstance({ key: 'prop', instanceOf: AClass });
```

Properties `Promise` values also have the same options.

```ts
test.hasPromiseValue({ key: 'prop', value: 'a' });
test.hasPromiseValueType({ key: 'prop', typeOf: 'number' });
test.hasPromiseValueInstance({ key: 'prop', instanceOf: AClass });
test.hasPromiseError({ key: 'prop', error: 'a' });
test.hasPromiseErrorType({ key: 'prop', typeOf: 'number' });
test.hasPromiseErrorInstance({ key: 'prop', instanceOf: AClass });
```

## Methods returned value

As with the properties, the returned values ​​of the methods can be verified. The helpers also verify that the key is the method of an object.

All methods have an **optional** object parameter called `args`, that's an `array` with all arguments to invoke the `key` method, in the same order they will be used.

```ts
test.returnsValue({ key: 'prop', value: 'a' });
test.returnsValue({ key: 'prop', value: 'a', args: ['a'] });
test.returnsValueType({ key: 'prop', typeOf: 'number' });
test.returnsValueInstance({ key: 'prop', instanceOf: AClass });
```

You can also verify the value issued by a returned `Observable` without creating a new one, simply by specifying the value issued, as well as its type or type of instance. Observable errors are also allowed to be tested.

```ts
test.returnsObservableValue({ key: 'prop', value: 'a' });
test.returnsObservableValueType({ key: 'prop', typeOf: 'number' });
test.returnsObservableValueInstance({ key: 'prop', instanceOf: AClass });
test.returnsObservableError({ key: 'prop', error: 'a' });
test.returnsObservableErrorType({ key: 'prop', typeOf: 'number' });
test.returnsObservableErrorInstance({ key: 'prop', instanceOf: AClass });
```

Returned `Promise` values also have the same options.

```ts
test.returnsPromiseValue({ key: 'prop', value: 'a' });
test.returnsPromiseValueType({ key: 'prop', typeOf: 'number' });
test.returnsPromiseValueInstance({ key: 'prop', instanceOf: AClass });
test.returnsPromiseError({ key: 'prop', error: 'a' });
test.returnsPromiseErrorType({ key: 'prop', typeOf: 'number' });
test.returnsPromiseErrorInstance({ key: 'prop', instanceOf: AClass });
```

## Spied members

This helpers let you spy that some methods have been called, with or without some args.

All helpers under this category have this object params: `{... fromKey: string, fromArgs?: any[] }`.

- **`fromKey`** is the key method that will fires the event to be spied, that uses to be the call to another method. This way we can even test that private or inherited methods are called.
- **`fromArgs`** are the arge needed to invoke `fromKey`.

The console helper let you test that a method sends a console message.

```ts
// Check that consoleLog('test') emits a console.log('test')
test.consoleIsCalled({
  level: 'log',
  args: ['test'],
  fromKey: 'consoleLog',
  fromArgs: ['test']
});
```

The method helper let you test a method, from the same or other object have been called.

```ts
// Check that privateMethod('a') is called from callMethod('a')
test.methodIsCalled({
  key: 'privateMethod',
  args: ['a'],
  fromKey: 'callMethod',
  fromArgs: ['a']
});

// Check that otherMethod(), from the otherObj object, is called from fireOther()
test.methodIsCalled({
  key: 'otherMethod',
  fromKey: 'fireOther',
  spiedObj: otherObj
});
```

The last option can be very helpful, for example, to test events methods.

```ts
const event: Event = new Event('a');

test.methodIsCalled({
  key: 'stopPropagation',
  fromKey: 'fireEvent',
  fromArgs: [event],
  spiedObj: event
});
```
