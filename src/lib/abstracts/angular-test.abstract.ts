import { Location } from '@angular/common';
import { Type } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { Event, NavigationEnd, Router, Routes } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { defaults, hasInjected, TestBedConfig, typeToString } from '@ngxa/utils';
import { filter } from 'rxjs/operators';
import { isUndefined } from 'util';

import { TestRouterCall, TestRouterRedirect } from '../typings/test-component.types';
import { ClassTestAbstract } from './class-test.abstract';

/**
 * Abstract class with methods to test basic Angular classes.
 */
export abstract class AngularTestAbstract<T extends object> extends ClassTestAbstract<T> {
  /**
   * Configuration and initialization of the test environment of the class.
   */
  testBed: typeof TestBed;
  /**
   * The browser URL interaction service.
   */
  location: Location;
  /**
   * The navigation service.
   */
  router: Router;

  /**
   * Creates an instance of a class with methods to test basic Angular classes.
   * @param clazz The class type to test.
   * @param [config] The custom module configuration needed to test the class.
   * @param args Extra args.
   */
  constructor(clazz: Type<T>, config: TestBedConfig = new TestBedConfig(), ...args: any[]) {
    super();
    this.testBed = this.setTestBed(clazz, config, ...args);
    this.location = this.testBed.get(Location);
    this.router = this.testBed.get(Router);
    this.router.initialNavigation();
  }

  /**
   * Set the testBed property.
   * @param clazz The class type to test.
   * @param config The custom module configuration needed to test the class.
   * @param [routes] The routes to test.
   * @param args Extra args.
   * @returns The configured TestBed object.
   */
  // tslint:disable-next-line:newspaper-order
  protected setTestBed(
    clazz: Type<T>,
    config: TestBedConfig,
    routes?: Routes,
    ...args: any[]
  ): typeof TestBed {
    if (isUndefined(routes)) {
      config.imports.push(RouterTestingModule);
    } else {
      config.imports.push(RouterTestingModule.withRoutes(routes));
    }

    return TestBed.configureTestingModule(config);
  }

  /**
   * Check if a module is imported.
   * @param clazz The module to test.
   */
  isImported(clazz: any): void {
    expect(this.testBed.get(clazz)).toBeTruthy(`The module ${typeToString(clazz)} is not imported`);
  }

  /**
   * Check if a module is not imported.
   * @param clazz The module to test.
   */
  isNotImported(clazz: any): void {
    expect(() => this.testBed.get(clazz)).toThrow();
  }

  /**
   * Check if a component is declares.
   * @param clazz The Angular element to test.
   */
  isComponentDeclared(clazz: any): void {
    expect(this.testBed.createComponent(clazz).componentInstance).toBeTruthy(
      `The class ${typeToString(clazz)} is not declared`
    );
  }

  /**
   * Check if a component is not declared.
   * @param clazz The Angular element to test.
   */
  isNotComponentDeclared(clazz: any): void {
    expect(() => this.testBed.createComponent(clazz).componentInstance).toThrow();
  }

  /**
   * Check if a service is provided.
   * @param clazz The service to test.
   */
  isProvided(clazz: any): void {
    expect(this.testBed.get(clazz)).toBeTruthy(
      `The service ${typeToString(clazz)} is not provided`
    );
  }

  /**
   * Check if a service is not provided.
   * @param clazz The service to test.
   */
  isNotProvided(clazz: any): void {
    expect(() => this.testBed.get(clazz)).toThrow();
  }

  /**
   * Check if the class is injected.
   * @param clazz The injected class.
   */
  isInjected(clazz: any): void {
    expect(hasInjected(this.obj, clazz)).toBeTruthy(
      `The object ${typeToString(this.obj)} has not the class ${typeToString(clazz)} injected`
    );
  }

  /**
   * Check if the class is not injected.
   * @param clazz The injected class.
   */
  isNotInjected(clazz: any): void {
    expect(hasInjected(this.obj, clazz)).toBeFalsy(
      `The object ${typeToString(this.obj)} has the class ${typeToString(clazz)} injected`
    );
  }

  /**
   * Check if navigate to an url on method call.
   * @param call The test router call object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  navigateOnCall<O extends object = T>(call: TestRouterCall, obj: T | O = this.obj): void {
    this.onNavigationEvent(NavigationEnd, (navigation: Event) => {
      expect((navigation as NavigationEnd).urlAfterRedirects).toBe(call.url);
    });
    (obj as any)[call.key](...defaults<any[]>(call.args, []));
  }

  /**
   * Check if do not navigate to an url on method call.
   * @param call The test router call object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  notNavigateOnCall<O extends object = T>(call: TestRouterCall, obj: T | O = this.obj): void {
    this.onNavigationEvent(NavigationEnd, (navigation: Event) => {
      expect((navigation as NavigationEnd).urlAfterRedirects).not.toBe(call.url);
    });
    (obj as any)[call.key](...defaults<any[]>(call.args, []));
  }

  /**
   * Check if navigate to an url on method call.
   * @param call The test router redirect call object.
   */
  routeRedirect(call: TestRouterRedirect): void {
    this.onNavigationEvent(NavigationEnd, (navigation: Event) => {
      expect((navigation as NavigationEnd).urlAfterRedirects).toBe(call.url);
    });
    this.router.navigate([call.initialUrl]);
  }

  /**
   * Check if do not navigate to an url on method call.
   * @param call The test router redirect call object.
   */
  routeNotRedirect(call: TestRouterRedirect): void {
    this.onNavigationEvent(NavigationEnd, (navigation: Event) => {
      expect((navigation as NavigationEnd).urlAfterRedirects).not.toBe(call.url);
    });
    this.router.navigate([call.initialUrl]);
  }

  /**
   * Check the function on navigation event.
   * @param event The router event to test.
   * @param fn The function to test on the event.
   */
  onNavigationEvent(event: any, fn: (navigation: Event) => void): void {
    this.router.events
      .pipe(filter((_event: Event) => _event instanceof event))
      .subscribe((navigation: Event) => {
        fn(navigation);
      });
  }

  /**
   * Check if a route exists.
   * @param route The route to check.
   */
  routeExists(route: string): void {
    this.router.navigate([route]).catch(() => {
      fail(`Route ${route} do not exists`);
    });
  }

  /**
   * Check if a route do not exists.
   * @param route The route to check.
   */
  routeNotExists(route: string): void {
    this.router.navigate([route]).then(() => {
      fail(`Route ${route} exists`);
    });
  }
}
