import { isPromise } from '@angular/compiler/src/util';
import {
  defaults,
  hasGettable,
  hasGetter,
  hasGetterSetter,
  hasMember,
  hasMethod,
  hasProperty,
  hasSettable,
  hasSetter,
  objectInheritsFrom,
  typeToString
} from '@ngxa/utils';
import { isObservable, Observable } from 'rxjs';

import {
  TestCall,
  TestCalled,
  TestConsole,
  TestFromCall,
  TestHasError,
  TestHasErrorInstance,
  TestHasErrorType,
  TestHasValue,
  TestHasValueInstance,
  TestHasValueType
} from '../typings/test-class.types';

/**
 * Abstract class with methods to test basic classes.
 */
export abstract class ClassTestAbstract<T extends object> {
  /**
   * The object to be tested.
   */
  obj!: T;

  /**
   * Check if the object is instantiable.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  isInstantiable<O extends object = T>(obj: T | O = this.obj): void {
    expect(obj).toBeTruthy(`The object "${typeToString(obj)}" should be instantiable`);
  }

  /**
   * Check if the object inherits from a class.
   * @param parent The class to test inheritance from.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  inheritsFrom<O extends object = T>(parent: any, obj: T | O = this.obj): void {
    expect(objectInheritsFrom(obj, parent)).toBeTruthy(
      `The object ${typeToString(obj)} do not inherits from ${typeToString(parent)}`
    );
  }

  /**
   * Check if the object do not inherits from a class.
   * @param parent The class to test inheritance from.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  notInheritsFrom<O extends object = T>(parent: any, obj: T | O = this.obj): void {
    expect(objectInheritsFrom(obj, parent)).toBeFalsy(
      `The object ${typeToString(obj)} inherits from ${typeToString(parent)}`
    );
  }

  /**
   * Check if the key is a defined object member.
   * @param key The key to test.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  isMember<O extends object = T>(key: PropertyKey, obj: T | O = this.obj): void {
    expect(hasMember(obj, key)).toBeTruthy(
      `The key "${key.toString()}" should be a member of "${typeToString(obj)}"`
    );
  }

  /**
   * Check if the key is not a defined object member.
   * @param key The key to test.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  isNotMember<O extends object = T>(key: PropertyKey, obj: T | O = this.obj): void {
    expect(hasMember(obj, key)).toBeFalsy(
      `The key "${key.toString()}" shouldn't be a member of "${typeToString(obj)}"`
    );
  }

  /**
   * Check if the key is a defined property.
   * @param key The key to test.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  isProperty<O extends object = T>(key: PropertyKey, obj: T | O = this.obj): void {
    expect(hasProperty(obj, key)).toBeTruthy(
      `The key "${key.toString()}" should be a property of "${typeToString(obj)}"`
    );
  }

  /**
   * Check if the key is not a defined property.
   * @param key The key to test.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  isNotProperty<O extends object = T>(key: PropertyKey, obj: T | O = this.obj): void {
    expect(hasProperty(obj, key)).toBeFalsy(
      `The key "${key.toString()}" shouldn't be a property of "${typeToString(obj)}"`
    );
  }

  /**
   * Check if the key is a getter or setter.
   * @param key The key to test.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  isGetterSetter<O extends object = T>(key: PropertyKey, obj: T | O = this.obj): void {
    expect(hasGetterSetter(obj, key)).toBeTruthy(
      `The key "${key.toString()}" should be a getter or setter property of "${typeToString(obj)}"`
    );
  }

  /**
   * Check if the key is not a getter or setter.
   * @param key The key to test.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  isNotGetterSetter<O extends object = T>(key: PropertyKey, obj: T | O = this.obj): void {
    expect(hasGetterSetter(obj, key)).toBeFalsy(
      `The key "${key.toString()}" shouldn't be a getter or setter property of "${typeToString(
        obj
      )}"`
    );
  }

  /**
   * Check if the key is gettable.
   * @param key The key to test.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  isGettable<O extends object = T>(key: PropertyKey, obj: T | O = this.obj): void {
    expect(hasGettable(obj, key)).toBeTruthy(
      `The key "${key.toString()}" should be a gettable property of "${typeToString(obj)}"`
    );
  }

  /**
   * Check if the key is not gettable.
   * @param key The key to test.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  isNotGettable<O extends object = T>(key: PropertyKey, obj: T | O = this.obj): void {
    expect(hasGettable(obj, key)).toBeFalsy(
      `The key "${key.toString()}" shouldn't be a gettable property of "${typeToString(obj)}"`
    );
  }

  /**
   * Check if the key is a getter.
   * @param key The key to test.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  isGetter<O extends object = T>(key: PropertyKey, obj: T | O = this.obj): void {
    expect(hasGetter(obj, key)).toBeTruthy(
      `The key "${key.toString()}" should be a getter property of "${typeToString(obj)}"`
    );
  }

  /**
   * Check if the key is not a getter.
   * @param key The key to test.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  isNotGetter<O extends object = T>(key: PropertyKey, obj: T | O = this.obj): void {
    expect(hasGetter(obj, key)).toBeFalsy(
      `The key "${key.toString()}" shouldn't be a getter property of "${typeToString(obj)}"`
    );
  }

  /**
   * Check if the key is settable.
   * @param key The key to test.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  isSettable<O extends object = T>(key: PropertyKey, obj: T | O = this.obj): void {
    expect(hasSettable(obj, key)).toBeTruthy(
      `The key "${key.toString()}" should be a settable property of "${typeToString(obj)}"`
    );
  }

  /**
   * Check if the key is not settable.
   * @param key The key to test.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  isNotSettable<O extends object = T>(key: PropertyKey, obj: T | O = this.obj): void {
    expect(hasSettable(obj, key)).toBeFalsy(
      `The key "${key.toString()}" shouldn't be a settable property of "${typeToString(obj)}"`
    );
  }

  /**
   * Check if the key is a setter.
   * @param key The key to test.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  isSetter<O extends object = T>(key: PropertyKey, obj: T | O = this.obj): void {
    expect(hasSetter(obj, key)).toBeTruthy(
      `The key "${key.toString()}" should be a setter property of "${typeToString(obj)}"`
    );
  }

  /**
   * Check if the key is not a setter.
   * @param key The key to test.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  isNotSetter<O extends object = T>(key: PropertyKey, obj: T | O = this.obj): void {
    expect(hasSetter(obj, key)).toBeFalsy(
      `The key "${key.toString()}" shouldn't be a setter property of "${typeToString(obj)}"`
    );
  }

  /**
   * Check if the key is a method.
   * @param key The key to test.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  isMethod<O extends object = T>(key: PropertyKey, obj: T | O = this.obj): void {
    expect(hasMethod(obj, key)).toBeTruthy(
      `The key "${key.toString()}" should be a method of "${typeToString(obj)}"`
    );
  }

  /**
   * Check if the key is not a method.
   * @param key The key to test.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  isNotMethod<O extends object = T>(key: PropertyKey, obj: T | O = this.obj): void {
    expect(hasMethod(obj, key)).toBeFalsy(
      `The key "${key.toString()}" shouldn't be a method of "${typeToString(obj)}"`
    );
  }

  /**
   * Check if the key is a gettable property and has a value.
   * @param call The member call value object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  hasValue<V, O extends object = T>(call: TestHasValue<V>, obj: T | O = this.obj): void {
    expect(this.getPropValue(call, obj)).toEqual(call.value);
  }

  /**
   * Check if the key is a gettable property and has not a value.
   * @param call The member call value object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  hasNotValue<V, O extends object = T>(call: TestHasValue<V>, obj: T | O = this.obj): void {
    expect(this.getPropValue(call, obj)).not.toEqual(call.value);
  }

  /**
   * Check if the key is a gettable property and has a value type.
   * @param call The member call value type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  hasValueType<V, O extends object = T>(call: TestHasValueType, obj: T | O = this.obj): void {
    const value: V = this.getPropValue(call, obj);

    expect(typeof value).toEqual(call.typeOf);
  }

  /**
   * Check if the key is a gettable property and has not a value type.
   * @param call The member call value type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  hasNotValueType<V, O extends object = T>(call: TestHasValueType, obj: T | O = this.obj): void {
    const value: V = this.getPropValue(call, obj);

    expect(typeof value).not.toEqual(call.typeOf);
  }

  /**
   * Check if the key is a gettable property and has a value instance type.
   * @param call The member call value instance object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  hasValueInstance<V, O extends object = T>(
    call: TestHasValueInstance,
    obj: T | O = this.obj
  ): void {
    const value: V = this.getPropValue(call, obj);

    expect(value instanceof call.instanceOf).toBeTruthy(
      `Expected property instance type to be "${typeToString(
        call.instanceOf
      )}", but is "${typeToString(value)}"`
    );
  }

  /**
   * Check if the key is a gettable property and has not a value instance type.
   * @param call The member call value instance object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  hasNotValueInstance<V, O extends object = T>(
    call: TestHasValueInstance,
    obj: T | O = this.obj
  ): void {
    const value: V = this.getPropValue(call, obj);

    expect(value instanceof call.instanceOf).toBeFalsy(
      `Expected property instance type not to be "${typeToString(call.instanceOf)}", but it is`
    );
  }

  /**
   * Check if the key is a gettable property and has an Observable value.
   * @param call The member call value object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  hasObservableValue<V, O extends object = T>(call: TestHasValue<V>, obj: T | O = this.obj): void {
    this.getObservablePropValue(call, obj).subscribe((next: V | {}) =>
      expect(next).toEqual(call.value)
    );
  }

  /**
   * Check if the key is a gettable property and has not an Observable value.
   * @param call The member call value object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  hasNotObservableValue<V, O extends object = T>(
    call: TestHasValue<V>,
    obj: T | O = this.obj
  ): void {
    this.getObservablePropValue(call, obj).subscribe((next: V | {}) =>
      expect(next).not.toEqual(call.value)
    );
  }

  /**
   * Check if the key is a gettable property and has an Observable value type.
   * @param call The member call value type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  hasObservableValueType<V, O extends object = T>(
    call: TestHasValueType,
    obj: T | O = this.obj
  ): void {
    this.getObservablePropValue(call, obj).subscribe((next: V | {}) =>
      expect(typeof next).toEqual(call.typeOf)
    );
  }

  /**
   * Check if the key is a gettable property and has not an Observable value type.
   * @param call The member call value type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  hasNotObservableValueType<V, O extends object = T>(
    call: TestHasValueType,
    obj: T | O = this.obj
  ): void {
    this.getObservablePropValue(call, obj).subscribe((next: V | {}) =>
      expect(typeof next).not.toEqual(call.typeOf)
    );
  }

  /**
   * Check if the key is a gettable property and has an Observable value instance type.
   * @param call The member call value instance object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  hasObservableValueInstance<V, O extends object = T>(
    call: TestHasValueInstance,
    obj: T | O = this.obj
  ): void {
    this.getObservablePropValue(call, obj).subscribe((next: V | {}) =>
      expect(next instanceof call.instanceOf).toBeTruthy(
        `Expected property Observable value instance type to be "${
          call.instanceOf
        }", but is "${typeToString(next)}"`
      )
    );
  }

  /**
   * Check if the key is a gettable property and has not an Observable value instance type.
   * @param call The member call value instance object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  hasNotObservableValueInstance<V, O extends object = T>(
    call: TestHasValueInstance,
    obj: T | O = this.obj
  ): void {
    this.getObservablePropValue(call, obj).subscribe((next: V | {}) =>
      expect(next instanceof call.instanceOf).toBeFalsy(
        `Expected property Observable value instance type not to be "${typeToString(
          call.instanceOf
        )}", but it is`
      )
    );
  }

  /**
   * Check if the key is a gettable property and has an Observable error.
   * @param call The member call error object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  hasObservableError<V, O extends object = T>(call: TestHasError<V>, obj: T | O = this.obj): void {
    this.getObservablePropValue(call, obj).subscribe({
      error: (error: V): void => {
        expect(error).toEqual(call.error);
      }
    });
  }

  /**
   * Check if the key is a gettable property and has not an Observable error.
   * @param call The member call error object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  hasNotObservableError<V, O extends object = T>(
    call: TestHasError<V>,
    obj: T | O = this.obj
  ): void {
    this.getObservablePropValue(call, obj).subscribe({
      error: (error: V): void => {
        expect(error).not.toEqual(call.error);
      }
    });
  }

  /**
   * Check if the key is a gettable property and has an Observable error type.
   * @param call The member call error type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  hasObservableErrorType<V, O extends object = T>(
    call: TestHasErrorType,
    obj: T | O = this.obj
  ): void {
    this.getObservablePropValue(call, obj).subscribe({
      error: (error: V): void => {
        expect(typeof error).toEqual(call.typeOf);
      }
    });
  }

  /**
   * Check if the key is a gettable property and has not an Observable error type.
   * @param call The member call error type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  hasNotObservableErrorType<V, O extends object = T>(
    call: TestHasErrorType,
    obj: T | O = this.obj
  ): void {
    this.getObservablePropValue(call, obj).subscribe({
      error: (error: V): void => {
        expect(typeof error).not.toEqual(call.typeOf);
      }
    });
  }

  /**
   * Check if the key is a gettable property and has an Observable error instance type.
   * @param call The member call error instance object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  hasObservableErrorInstance<V, O extends object = T>(
    call: TestHasErrorInstance,
    obj: T | O = this.obj
  ): void {
    this.getObservablePropValue(call, obj).subscribe({
      error: (error: V): void => {
        expect(error instanceof call.instanceOf).toBeTruthy(
          `Expected property Observable error instance type to be "${
            call.instanceOf
          }", but is "${typeToString(error)}"`
        );
      }
    });
  }

  /**
   * Check if the key is a gettable property and has not an Observable error instance type.
   * @param call The member call error instance object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  hasNotObservableErrorInstance<V, O extends object = T>(
    call: TestHasErrorInstance,
    obj: T | O = this.obj
  ): void {
    this.getObservablePropValue(call, obj).subscribe({
      error: (error: V): void => {
        expect(error instanceof call.instanceOf).toBeFalsy(
          `Expected property Observable error instance type not to be "${typeToString(
            call.instanceOf
          )}", but it is`
        );
      }
    });
  }

  /**
   * Get the observable value from a property.
   * @param call The member call object.
   * @param obj The object to test.
   * @returns The value of the property as Observable.
   */
  protected getObservablePropValue<V, O extends object>(call: TestCall, obj: O): Observable<V> {
    const observable: Observable<V> = this.getPropValue(call, obj);

    expect(isObservable(observable)).toBeTruthy(
      `"${typeToString(obj)}.${call.key.toString()}" should be "Observable", but is "${typeToString(
        observable
      )}"`
    );

    return observable;
  }

  /**
   * Check if the key is a gettable property and has a Promise value.
   * @param call The member call value object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  hasPromiseValue<V, O extends object = T>(call: TestHasValue<V>, obj: T | O = this.obj): void {
    this.getPromisePropValue(call, obj).then((next: V | {}) => expect(next).toEqual(call.value));
  }

  /**
   * Check if the key is a gettable property and has not a Promise value.
   * @param call The member call value object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  hasNotPromiseValue<V, O extends object = T>(call: TestHasValue<V>, obj: T | O = this.obj): void {
    this.getPromisePropValue(call, obj).then((next: V | {}) =>
      expect(next).not.toEqual(call.value)
    );
  }

  /**
   * Check if the key is a gettable property and has a Promise value type.
   * @param call The member call value type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  hasPromiseValueType<V, O extends object = T>(
    call: TestHasValueType,
    obj: T | O = this.obj
  ): void {
    this.getPromisePropValue(call, obj).then((next: V | {}) =>
      expect(typeof next).toEqual(call.typeOf)
    );
  }

  /**
   * Check if the key is a gettable property and has not a Promise value type.
   * @param call The member call value type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  hasNotPromiseValueType<V, O extends object = T>(
    call: TestHasValueType,
    obj: T | O = this.obj
  ): void {
    this.getPromisePropValue(call, obj).then((next: V | {}) =>
      expect(typeof next).not.toEqual(call.typeOf)
    );
  }

  /**
   * Check if the key is a gettable property and has a Promise value instance type.
   * @param call The member call value instance object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  hasPromiseValueInstance<V, O extends object = T>(
    call: TestHasValueInstance,
    obj: T | O = this.obj
  ): void {
    this.getPromisePropValue(call, obj).then((next: V | {}) =>
      expect(next instanceof call.instanceOf).toBeTruthy(
        `Expected property Promise value instance type to be "${typeToString(
          call.instanceOf
        )}", but is "${typeToString(next)}"`
      )
    );
  }

  /**
   * Check if the key is a gettable property and has not a Promise value instance type.
   * @param call The member call value instance object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  hasNotPromiseValueInstance<V, O extends object = T>(
    call: TestHasValueInstance,
    obj: T | O = this.obj
  ): void {
    this.getPromisePropValue(call, obj).then((next: V | {}) =>
      expect(next instanceof call.instanceOf).toBeFalsy(
        `Expected property Promise value instance type not to be "${typeToString(
          call.instanceOf
        )}", but it is`
      )
    );
  }

  /**
   * Check if the key is a gettable property and has a Promise error.
   * @param call The member call error object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  hasPromiseError<V, O extends object = T>(call: TestHasError<V>, obj: T | O = this.obj): void {
    this.getPromisePropValue(call, obj).catch((error: V) => expect(error).toEqual(call.error));
  }

  /**
   * Check if the key is a gettable property and has a Promise error.
   * @param call The member call error object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  hasNotPromiseError<V, O extends object = T>(call: TestHasError<V>, obj: T | O = this.obj): void {
    this.getPromisePropValue(call, obj).catch((error: V) => expect(error).not.toEqual(call.error));
  }

  /**
   * Check if the key is a gettable property and has a Promise error type.
   * @param call The member call error type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  hasPromiseErrorType<V, O extends object = T>(
    call: TestHasErrorType,
    obj: T | O = this.obj
  ): void {
    this.getPromisePropValue(call, obj).catch((error: V) =>
      expect(typeof error).toEqual(call.typeOf)
    );
  }

  /**
   * Check if the key is a gettable property and has not a Promise error type.
   * @param call The member call error type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  hasNotPromiseErrorType<V, O extends object = T>(
    call: TestHasErrorType,
    obj: T | O = this.obj
  ): void {
    this.getPromisePropValue(call, obj).catch((error: V) =>
      expect(typeof error).not.toEqual(call.typeOf)
    );
  }

  /**
   * Check if the key is a gettable property and has a Promise error instance type.
   * @param call The member call error instance object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  hasPromiseErrorInstance<V, O extends object = T>(
    call: TestHasErrorInstance,
    obj: T | O = this.obj
  ): void {
    this.getPromisePropValue(call, obj).catch((error: V) =>
      expect(error instanceof call.instanceOf).toBeTruthy(
        `Expected property Promise error instance type to be "${typeToString(
          call.instanceOf
        )}", but is "${typeToString(error)}"`
      )
    );
  }

  /**
   * Check if the key is a gettable property and has not a Promise error instance type.
   * @param call The member call error instance object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  hasNotPromiseErrorInstance<V, O extends object = T>(
    call: TestHasErrorInstance,
    obj: T | O = this.obj
  ): void {
    this.getPromisePropValue(call, obj).catch((error: V) =>
      expect(error instanceof call.instanceOf).toBeFalsy(
        `Expected property Promise error instance type not to be "${typeToString(
          call.instanceOf
        )}", but it is`
      )
    );
  }

  /**
   * Get the promise value from a property.
   * @param call The member call object.
   * @param obj The object to test.
   * @returns The value of the property as promise.
   */
  protected async getPromisePropValue<V, O extends object>(call: TestCall, obj: O): Promise<V> {
    const promise: Promise<V> = this.getPropValue(call, obj);

    expect(isPromise(promise)).toBeTruthy(
      `"${typeToString(obj)}.${call.key.toString()}" should be "Promise", but is "${typeToString(
        promise
      )}"`
    );

    return promise;
  }

  /**
   * Check if the method returns a value.
   * @param call The member call value object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  returnsValue<V, O extends object = T>(call: TestHasValue<V>, obj: T | O = this.obj): void {
    expect(this.getMethodValue(call, obj)).toEqual(call.value);
  }

  /**
   * Check if the method do not returns a value.
   * @param call The member call value object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  returnsNotValue<V, O extends object = T>(call: TestHasValue<V>, obj: T | O = this.obj): void {
    expect(this.getMethodValue(call, obj)).not.toEqual(call.value);
  }

  /**
   * Check if the method returns a value type.
   * @param call The member call value type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  returnsValueType<V, O extends object = T>(call: TestHasValueType, obj: T | O = this.obj): void {
    const value: V = this.getMethodValue(call, obj);

    expect(typeof value).toEqual(call.typeOf);
  }

  /**
   * Check if the method returns not a value type.
   * @param call The member call value type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  returnsNotValueType<V, O extends object = T>(
    call: TestHasValueType,
    obj: T | O = this.obj
  ): void {
    const value: V = this.getMethodValue(call, obj);

    expect(typeof value).not.toEqual(call.typeOf);
  }

  /**
   * Check if the method returns a value instance type.
   * @param call The member call value instance object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  returnsValueInstance<V, O extends object = T>(
    call: TestHasValueInstance,
    obj: T | O = this.obj
  ): void {
    const value: V = this.getMethodValue(call, obj);

    expect(value instanceof call.instanceOf).toBeTruthy(
      `Expected method returned value instance type to be "${typeToString(
        call.instanceOf
      )}", but is "${typeToString(value)}"`
    );
  }

  /**
   * Check if the method returns not a value instance type.
   * @param call The member call value instance object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  returnsNotValueInstance<V, O extends object = T>(
    call: TestHasValueInstance,
    obj: T | O = this.obj
  ): void {
    const value: V = this.getMethodValue(call, obj);

    expect(value instanceof call.instanceOf).toBeFalsy(
      `Expected method returned value instance type not to be "${typeToString(
        call.instanceOf
      )}", but it is`
    );
  }

  /**
   * Check if the method returns an observable value.
   * @param call The member call value object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  returnsObservableValue<V, O extends object = T>(
    call: TestHasValue<V>,
    obj: T | O = this.obj
  ): void {
    this.getObservableMethodValue(call, obj).subscribe((next: V | {}) =>
      expect(next).toEqual(call.value)
    );
  }

  /**
   * Check if the method do not returns an observable value.
   * @param call The member call value object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  returnsNotObservableValue<V, O extends object = T>(
    call: TestHasValue<V>,
    obj: T | O = this.obj
  ): void {
    this.getObservableMethodValue(call, obj).subscribe((next: V | {}) =>
      expect(next).not.toEqual(call.value)
    );
  }

  /**
   * Check if the method returns an observable value type.
   * @param call The member call value type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  returnsObservableValueType<V, O extends object = T>(
    call: TestHasValueType,
    obj: T | O = this.obj
  ): void {
    this.getObservableMethodValue(call, obj).subscribe((next: V | {}) =>
      expect(typeof next).toEqual(call.typeOf)
    );
  }

  /**
   * Check if the method returns not an observable value type.
   * @param call The member call value type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  returnsNotObservableValueType<V, O extends object = T>(
    call: TestHasValueType,
    obj: T | O = this.obj
  ): void {
    this.getObservableMethodValue(call, obj).subscribe((next: V | {}) =>
      expect(typeof next).not.toEqual(call.typeOf)
    );
  }

  /**
   * Check if the method returns an observable value instance type.
   * @param call The member call value instance object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  returnsObservableValueInstance<V, O extends object = T>(
    call: TestHasValueInstance,
    obj: T | O = this.obj
  ): void {
    this.getObservableMethodValue(call, obj).subscribe((next: V | {}) =>
      expect(next instanceof call.instanceOf).toBeTruthy(
        `Expected method returned Observable value instance type to be "${typeToString(
          call.instanceOf
        )}", but is "${typeToString(next)}"`
      )
    );
  }

  /**
   * Check if the method returns not an observable value instance type.
   * @param call The member call value instance object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  returnsNotObservableValueInstance<V, O extends object = T>(
    call: TestHasValueInstance,
    obj: T | O = this.obj
  ): void {
    this.getObservableMethodValue(call, obj).subscribe((next: V | {}) =>
      expect(next instanceof call.instanceOf).toBeFalsy(
        `Expected method returned Observable value instance type not to be "${typeToString(
          call.instanceOf
        )}", but it is`
      )
    );
  }

  /**
   * Check if the method returns an observable error.
   * @param call The member call error object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  returnsObservableError<V, O extends object = T>(
    call: TestHasError<V>,
    obj: T | O = this.obj
  ): void {
    this.getObservableMethodValue(call, obj).subscribe({
      error: (error: V): void => {
        expect(error).toEqual(call.error);
      }
    });
  }

  /**
   * Check if the method do not returns an observable error.
   * @param call The member call error object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  returnsNotObservableError<V, O extends object = T>(
    call: TestHasError<V>,
    obj: T | O = this.obj
  ): void {
    this.getObservableMethodValue(call, obj).subscribe({
      error: (error: V): void => {
        expect(error).not.toEqual(call.error);
      }
    });
  }

  /**
   * Check if the method returns an observable error type.
   * @param call The member call error type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  returnsObservableErrorType<V, O extends object = T>(
    call: TestHasErrorType,
    obj: T | O = this.obj
  ): void {
    this.getObservableMethodValue(call, obj).subscribe({
      error: (error: V): void => {
        expect(typeof error).toEqual(call.typeOf);
      }
    });
  }

  /**
   * Check if the method returns not an observable error type.
   * @param call The member call error type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  returnsNotObservableErrorType<V, O extends object = T>(
    call: TestHasErrorType,
    obj: T | O = this.obj
  ): void {
    this.getObservableMethodValue(call, obj).subscribe({
      error: (error: V): void => {
        expect(typeof error).not.toEqual(call.typeOf);
      }
    });
  }

  /**
   * Check if the method returns an observable error instance type.
   * @param call The member call error instance object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  returnsObservableErrorInstance<V, O extends object = T>(
    call: TestHasErrorInstance,
    obj: T | O = this.obj
  ): void {
    this.getObservableMethodValue(call, obj).subscribe({
      error: (error: V): void => {
        expect(error instanceof call.instanceOf).toBeTruthy(
          `Expected method returned Observable error instance type to be "${typeToString(
            call.instanceOf
          )}", but is "${typeToString(error)}"`
        );
      }
    });
  }

  /**
   * Check if the method returns not an observable error instance type.
   * @param call The member call error instance object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  returnsNotObservableErrorInstance<V, O extends object = T>(
    call: TestHasErrorInstance,
    obj: T | O = this.obj
  ): void {
    this.getObservableMethodValue(call, obj).subscribe({
      error: (error: V): void => {
        expect(error instanceof call.instanceOf).toBeFalsy(
          `Expected method returned Observable error instance type not to be "${typeToString(
            call.instanceOf
          )}", but it is`
        );
      }
    });
  }

  /**
   * Get the observable value from a method call.
   * @param call The member call object.
   * @param obj The object to test.
   * @returns The value returned by the method as Observable.
   */
  protected getObservableMethodValue<V, O extends object>(call: TestCall, obj: O): Observable<V> {
    const observable: Observable<V> = this.getMethodValue(call, obj);

    expect(isObservable(observable)).toBeTruthy(
      `"${typeToString(obj)}.${call.key.toString()}(${defaults<any[]>(call.args, []).join(
        ', '
      )})" should returns "Observable", but returns "${typeToString(observable)}"`
    );

    return observable;
  }

  /**
   * Check if the method returns a Promise value.
   * @param call The member call value object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  returnsPromiseValue<V, O extends object = T>(call: TestHasValue<V>, obj: T | O = this.obj): void {
    this.getPromiseMethodValue(call, obj).then((value: V | {}) =>
      expect(value).toEqual(call.value)
    );
  }

  /**
   * Check if the method do not returns a Promise value.
   * @param call The member call value object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  returnsNotPromiseValue<V, O extends object = T>(
    call: TestHasValue<V>,
    obj: T | O = this.obj
  ): void {
    this.getPromiseMethodValue(call, obj).then((value: V | {}) =>
      expect(value).not.toEqual(call.value)
    );
  }

  /**
   * Check if the method returns a Promise value type.
   * @param call The member call value type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  returnsPromiseValueType<V, O extends object = T>(
    call: TestHasValueType,
    obj: T | O = this.obj
  ): void {
    this.getPromiseMethodValue(call, obj).then((value: V | {}) =>
      expect(typeof value).toEqual(call.typeOf)
    );
  }

  /**
   * Check if the method returns not a Promise value type.
   * @param call The member call value type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  returnsNotPromiseValueType<V, O extends object = T>(
    call: TestHasValueType,
    obj: T | O = this.obj
  ): void {
    this.getPromiseMethodValue(call, obj).then((value: V | {}) =>
      expect(typeof value).not.toEqual(call.typeOf)
    );
  }

  /**
   * Check if the method returns a Promise value instance type.
   * @param call The member call value instance object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  returnsPromiseValueInstance<V, O extends object = T>(
    call: TestHasValueInstance,
    obj: T | O = this.obj
  ): void {
    this.getPromiseMethodValue(call, obj).then((value: V | {}) =>
      expect(value instanceof call.instanceOf).toBeTruthy(
        `Expected method returned Promise value instance type to be "${typeToString(
          call.instanceOf
        )}", but is "${typeToString(value)}"`
      )
    );
  }

  /**
   * Check if the method returns not a Promise value instance type.
   * @param call The member call value instance object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  returnsNotPromiseValueInstance<V, O extends object = T>(
    call: TestHasValueInstance,
    obj: T | O = this.obj
  ): void {
    this.getPromiseMethodValue(call, obj).then((value: V | {}) =>
      expect(value instanceof call.instanceOf).toBeFalsy(
        `Expected method returned Promise value instance type not to be "${typeToString(
          call.instanceOf
        )}", but it is`
      )
    );
  }

  /**
   * Check if the method returns a Promise error.
   * @param call The member call error object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  returnsPromiseError<V, O extends object = T>(call: TestHasError<V>, obj: T | O = this.obj): void {
    this.getPromiseMethodValue(call, obj).catch((error: V) => expect(error).toEqual(call.error));
  }

  /**
   * Check if the method do not returns a Promise error.
   * @param call The member call error object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  returnsNotPromiseError<V, O extends object = T>(
    call: TestHasError<V>,
    obj: T | O = this.obj
  ): void {
    this.getPromiseMethodValue(call, obj).catch((error: V) =>
      expect(error).not.toEqual(call.error)
    );
  }

  /**
   * Check if the method returns a Promise error type.
   * @param call The member call error type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  returnsPromiseErrorType<V, O extends object = T>(
    call: TestHasErrorType,
    obj: T | O = this.obj
  ): void {
    this.getPromiseMethodValue(call, obj).catch((error: V) =>
      expect(typeof error).toEqual(call.typeOf)
    );
  }

  /**
   * Check if the method returns not a Promise error type.
   * @param call The member call error type object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  returnsNotPromiseErrorType<V, O extends object = T>(
    call: TestHasErrorType,
    obj: T | O = this.obj
  ): void {
    this.getPromiseMethodValue(call, obj).catch((error: V) =>
      expect(typeof error).not.toEqual(call.typeOf)
    );
  }

  /**
   * Check if the method returns a Promise error instance type.
   * @param call The member call error instance object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  returnsPromiseErrorInstance<V, O extends object = T>(
    call: TestHasErrorInstance,
    obj: T | O = this.obj
  ): void {
    this.getPromiseMethodValue(call, obj).catch((error: V) =>
      expect(error instanceof call.instanceOf).toBeTruthy(
        `Expected method returned Promise error instance type to be "${typeToString(
          call.instanceOf
        )}", but is "${typeToString(error)}"`
      )
    );
  }

  /**
   * Check if the method returns not a Promise error instance type.
   * @param call The member call error instance object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  returnsNotPromiseErrorInstance<V, O extends object = T>(
    call: TestHasErrorInstance,
    obj: T | O = this.obj
  ): void {
    this.getPromiseMethodValue(call, obj).catch((error: V) =>
      expect(error instanceof call.instanceOf).toBeFalsy(
        `Expected method returned Promise error instance type not to be "${typeToString(
          call.instanceOf
        )}", but it is`
      )
    );
  }

  /**
   * Get the promise value from a method call.
   * @param call The member call object.
   * @param obj The object to test.
   * @returns The value returned by the method as promise.
   */
  protected async getPromiseMethodValue<V, O extends object>(call: TestCall, obj: O): Promise<V> {
    const promise: Promise<V> = this.getMethodValue(call, obj);

    expect(isPromise(promise)).toBeTruthy(
      `"${typeToString(obj)}.${call.key.toString()}(${defaults<any[]>(call.args, []).join(
        ', '
      )})" should returns "Promise", but returns "${typeToString(promise)}"`
    );

    return promise;
  }

  /**
   * Check if a console output has been called.
   * @param call The test console object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  consoleIsCalled<O extends object = T>(call: TestConsole, obj: T | O = this.obj): void {
    spyOn(console, call.level);
    expect(console[call.level]).not.toHaveBeenCalled();
    this.callFromKey(call, obj);
    expect(console[call.level]).toHaveBeenCalledWith(...call.args);
  }

  /**
   * Check if a console output has not been called.
   * @param call The test console object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  consoleIsNotCalled<O extends object = T>(call: TestConsole, obj: T | O = this.obj): void {
    spyOn(console, call.level);
    expect(console[call.level]).not.toHaveBeenCalled();
    this.callFromKey(call, obj);
    expect(console[call.level]).not.toHaveBeenCalledWith(...call.args);
  }

  /**
   * Check if a method has been called.
   * @param call The member called object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  methodIsCalled<O extends object = T>(call: TestCalled<O>, obj: T | O = this.obj): void {
    this.isCalled(
      call,
      obj,
      (): void => {
        this.callFromKey(call, obj);
      }
    );
  }

  /**
   * Check if a method has been called from function.
   * @param call The member called object.
   * @param obj The object to test.
   * @param fn The function to call.
   */
  protected isCalled<O extends object = T>(call: TestCalled<O>, obj: O, fn: () => void): void {
    const spiedObj: T | O = defaults(call.spiedObj, obj);
    const key: keyof T & keyof O = call.key as keyof T & keyof O;

    this.isMethod(key, spiedObj);
    spyOn(spiedObj, key);
    expect(spiedObj[key]).not.toHaveBeenCalled();
    fn();
    if (call.args === undefined) {
      expect(spiedObj[key]).toHaveBeenCalled();
    } else {
      expect(spiedObj[key]).toHaveBeenCalledWith(...call.args);
    }
  }

  /**
   * Check if a method has not been called.
   * @param call The member called object.
   * @param [obj=this.obj] The object to test. Defaults to the test `obj`.
   */
  methodIsNotCalled<O extends object = T>(call: TestCalled<O>, obj: T | O = this.obj): void {
    this.isNotCalled(
      call,
      obj,
      (): void => {
        this.callFromKey(call, obj);
      }
    );
  }

  /**
   * Check if a method has not been called from function.
   * @param call The member called object.
   * @param obj The object to test.
   * @param fn The function to call.
   */
  protected isNotCalled<O extends object = T>(call: TestCalled<O>, obj: O, fn: () => void): void {
    const spiedObj: T | O = defaults(call.spiedObj, obj);
    const key: keyof T & keyof O = call.key as keyof T & keyof O;

    this.isMethod(key, spiedObj);
    spyOn(spiedObj, key);
    expect(spiedObj[key]).not.toHaveBeenCalled();
    fn();
    if (call.args === undefined) {
      expect(spiedObj[key]).not.toHaveBeenCalled();
    } else {
      expect(spiedObj[key]).not.toHaveBeenCalledWith(...call.args);
    }
  }

  /**
   * Returns the value resulting from call a property.
   * @param call The member call object.
   * @param obj The object to test.
   * @returns The value resulting from call a property.
   */
  protected getPropValue<V>(call: TestCall, obj: object): V {
    this.isGettable(call.key, obj);

    return (obj as any)[call.key];
  }

  /**
   * Returns the value resulting from call a method.
   * @param call The member call object.
   * @param obj The object to test.
   * @returns The value resulting from call a method.
   */
  protected getMethodValue<V>(call: TestCall, obj: object): V {
    this.isMethod(call.key, obj);

    return (obj as any)[call.key](...defaults<any[]>(call.args, []));
  }

  /**
   * Call a method before the member call to test.
   * @param call The member call object.
   * @param obj The object to test.
   */
  protected callFromKey<O extends object>(call: TestFromCall, obj: O): void {
    this.isMethod(call.fromKey, obj);
    (obj as any)[call.fromKey](...defaults<any[]>(call.fromArgs, []));
  }
}
