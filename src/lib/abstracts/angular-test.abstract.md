# Basic Angular helpers

This helpers can be used to test any type of Angular element.

Any class that extends this one must set `TestBed`. They could add a third option `routes` to test the project routes with the `Router` and `Location` preinjected services.

## TestBed helpers

This helpers let you assert that some elements are in TestBed.

```ts
test.isImported(AModule);
test.isComponentDeclared(AComponent);
test.isProvided(AService);
```

## Class helpers

This helper let you assert that a service is injected.

```ts
test.isInjected(AService);
```

## Methods helpers

Whith this helpers we test that once a method is fired a router nagigation is done, specifying where it gone. It also offer a helper to test navigation events and to check redirections and existence.

```ts
test.navigateOnCall({ key: 'navigate', args: ['a'], url: '/search'})
test.onNavigationEvent(NavigationEnd, (navigation: Event) => {
  expect(...);
});
test.routeRedirect({ initialUrl: '/', url: '/home' });
test.routeExists('/home');
```
