import { Type } from '@angular/core';

import { TestCall, TestValue } from './test-class.types';

/**
 * The test by selector object.
 */
export interface TestBySelector {
  /**
   * The selector to check.
   */
  selector: string;
}

/**
 * Check that the value implemens TestBySelector.
 *
 * @param value The value to check.
 * @returns True as type guard if implements TestBySelector, false otherwise.
 */
export function isTestBySelector(value: any): value is TestBySelector {
  return typeof value === 'object' && value.selector !== undefined;
}

/**
 * The test by directive object.
 */
export interface TestByDirective<T> {
  /**
   * The directive to check.
   */
  directive: Type<T>;
}

/**
 * The test position object.
 */
export interface TestPosition {
  /**
   * The position, starting in 1.
   */
  nth?: number;
}

/**
 * The test element by object.
 */
export type TestElementBy<T> = TestBySelector | TestByDirective<T>;

/**
 * The test element object.
 */
export type TestElement<T> = TestElementBy<T> & TestPosition;

/**
 * The test selector value object.
 */
export type TestElementValue<T> = TestElement<T> & TestValue<string | RegExp>;

/**
 * The event call object.
 */
export interface TestEvent<T> {
  /**
   * The member key to check.
   */
  key: PropertyKey;
  /**
   * The event name to check.
   */
  eventName: string;
  /**
   * The event object to check.
   */
  eventObj?: T;
}

/**
 * The test event call object.
 */
export type TestEventCall<O, T> = TestEvent<O> & TestElement<T>;

/**
 * The test attribute object.
 */
export interface TestElementAttribute {
  /**
   * The attribute to check.
   */
  attribute: string;
}

/**
 * The test attribute object.
 */
export type TestAttribute<T> = TestElement<T> & TestElementAttribute;

/**
 * The test attribute value object.
 */
export type TestAttributeValue<T> = TestAttribute<T> & TestValue<string | RegExp>;

/**
 * The test property class.
 */
export interface TestElementClass {
  /**
   * The class to check.
   */
  cssClass: string;
}

/**
 * The test css class object.
 */
export type TestCssClass<T> = TestElement<T> & TestElementClass;

/**
 * The test router url object.
 */
export interface TestRouterUrl {
  /**
   * The url to check.
   */
  url: string;
  /**
   * The optional initial url to check.
   */
  initialUrl?: string;
}

/**
 * The test router call class object.
 */
export type TestRouterCall = TestCall & TestRouterUrl;

/**
 * The test router redirect url object.
 */
export interface TestRouterRedirect extends TestRouterUrl {
  /**
   * The optional initial url to check.
   */
  initialUrl: string;
}
