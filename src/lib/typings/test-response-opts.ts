import { HttpHeaders } from '@angular/common/http';

import { TestErrorResponse, TestResponse } from './test-http.types';

/**
 * Construct the options to build a mocked HTTP response.
 *
 * @param call The HTTP response call.
 * @returns The opts to mock an HTTP response.
 */
export function testResponseOpts<T>(
  call: TestResponse<T> | TestErrorResponse
): {
  headers?: HttpHeaders | { [name: string]: string | string[] };
  status?: number;
  statusText?: string;
} {
  return {
    headers: call.response.headers,
    status: call.response.status,
    statusText: call.response.statusText
  };
}
